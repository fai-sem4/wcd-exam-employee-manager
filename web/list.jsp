<%-- 
    Document   : list
    Created on : Jun 11, 2019, 3:07:38 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>List employee!</h1>
        <jsp:useBean id="empmodel" class="com.example.model.EmployeeModel" scope="request" />
            <c:forEach var="employee" items="${empmodel.getAllEmployee()}">
                <c:out value="${employee.id}"/>
                <c:out value="${employee.fullname}"/>
                <c:out value="${employee.birthday}"/>
                <c:out value="${employee.address}"/>
                <c:out value="${employee.position}"/>
                <c:out value="${employee.department}"/>
            </c:forEach>
    </body>
</html>
