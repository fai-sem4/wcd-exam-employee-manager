<%-- 
    Document   : index
    Created on : Jun 11, 2019, 2:12:38 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Employee JSP Page</title>
    </head>
    <body>
        <h1>New Employee!</h1>
        <form action="ServletController" method="POST">
            Full Name: <input type="text" name="fullname"></br>
            Birthday: <input type="text" name="birthday"></br>
            Address: <input type="text" name="address"></br>
            Position: <input type="text" name="position"></br>
            Department: <input type="text" name="department"></br>
            <input type="submit" value="Submit"></br>
            <INPUT TYPE="RESET" value="Reset">
        </form>
    </body>
</html>
