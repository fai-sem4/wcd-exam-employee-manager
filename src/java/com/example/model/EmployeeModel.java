/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.model;

import com.example.entity.Employee;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Admin
 */
public class EmployeeModel {
    private List<Employee> employees = null;
    private PreparedStatement pstm;
    private static Connection connection;
    
    public static Connection getConnection() throws SQLException {
        if (connection == null){
            connection = DriverManager.getConnection("jdbc:derby://localhost:1527/person;user=sa;password=sa");
        }
        return connection;
    }
    
    public int createEmployee(Employee emp) throws SQLException{
        connection = EmployeeModel.getConnection();
        pstm = connection.prepareStatement("INSERT INTO EMPLOYEE (id, fullname, birthday, address, position, department) values (?, ?, ?, ?, ?, ?)");
        pstm.setInt(1, emp.getId());
        pstm.setString(2, emp.getFullname());
        pstm.setString(3, emp.getBirthday());
        pstm.setString(4, emp.getAddress());
        pstm.setString(5, emp.getPosition());
        pstm.setString(6, emp.getDepartment());
        int r = pstm.executeUpdate();
        return r;
    }
    
    public List<Employee> getAllEmployee() throws SQLException{
        connection = EmployeeModel.getConnection();
        pstm = connection.prepareStatement("SELECT * FROM EMPLOYEE");
        ResultSet rs = pstm.executeQuery();
        employees = new LinkedList<>();
        while(rs.next()){
            employees.add(new Employee(rs.getInt("id"), rs.getString("fullname"), rs.getString("birthday"), rs.getString("address"), rs.getString("position"), rs.getString("department")));
        }
        return employees;
    }
}
